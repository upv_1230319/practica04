package course.examples.practica04;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TabHost;
import android.widget.TextView;


public class MainActivity extends ActionBarActivity {
    //variables area
    public double Lado;
    public EditText edit_Lado;
    public TextView Resultado;
    //variables fahr-celsius
    private EditText cent, far;
    //variables millas-km
    public double millas,km;
    public EditText edit_mill,edit_km;
    public TextView Res;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //area del cuadrado
        edit_Lado = (EditText) findViewById(R.id.editText1);
        Resultado = (TextView) findViewById(R.id.text_View3);

        //longitud
        edit_mill = (EditText) findViewById(R.id.txtMillas);
        Res = (TextView) findViewById(R.id.txtRes);


        TabHost tabHost=(TabHost)findViewById(R.id.tabHost);
        tabHost.setup();

        TabHost.TabSpec spec1=tabHost.newTabSpec("Tab 1");
        spec1.setContent(R.id.tab1);
        spec1.setIndicator("Temperatura");

        TabHost.TabSpec spec2=tabHost.newTabSpec("Tab 2");
        spec2.setContent(R.id.tab2);
        spec2.setIndicator("Area");


        TabHost.TabSpec spec3=tabHost.newTabSpec("Tab 3");
        spec3.setContent(R.id.tab3);
        spec3.setIndicator("Longitud");


        tabHost.addTab(spec1);
        tabHost.addTab(spec2);
        tabHost.addTab(spec3);

        //Fahrenheit a celsius
        this.cent = (EditText)findViewById(R.id.txtCentigrados);
        this.far = (EditText)findViewById(R.id.txtFahrenheit);

        this.cent.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                float faren;
                faren = (1.8f) * Float.parseFloat(cent.getText().toString()) + 32;
                far.setText("" + faren);

                return false;
            }
        });


        this.far.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                float centi;
                centi = (Float.parseFloat(far.getText().toString()) - 32) / (1.8f);
                cent.setText("" + centi);

                return false;
            }

        });

    }

    public void calcularArea(View view){
        Lado = Double.parseDouble(edit_Lado.getText().toString());
        Resultado.setText(Double.toString(Lado * Lado));
    }

    public void calcularLongitud(View view){
        millas = Double.parseDouble(edit_mill.getText().toString());
        Res.setText(Double.toString(millas * 1.609));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}